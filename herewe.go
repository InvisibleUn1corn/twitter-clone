package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"text/template"
	"time"

	"gitlab.com/InvisibleUn1corn/image-board.git/db"
	"gitlab.com/InvisibleUn1corn/image-board.git/posts"
	"gitlab.com/InvisibleUn1corn/image-board.git/users"

	"github.com/gorilla/mux"
)

type page struct {
	Name string
}

type loginPage struct {
	Name  string
	Error string
}

type indexPage struct {
	Name  string
	User  users.User
	Posts []posts.Post
}

type profilePage struct {
	Name        string
	ProfileUser users.User
	User        users.User
}

var templates = template.Must(template.ParseGlob("views/*"))

func indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Process index " + r.Method + " request from " + r.RemoteAddr)
	user, err := users.ValidateTokenMiddleware(w, r)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/login", 303)
		return
	}

	posts, err := posts.GetPosts(50, user.ID)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/500", 500)
		return
	}

	err = templates.ExecuteTemplate(w, "Timeline", indexPage{Name: "Timeline", User: user, Posts: posts})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func tweetHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Process index " + r.Method + " request from " + r.RemoteAddr)
	err := r.ParseForm()

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user, err := users.ValidateTokenMiddleware(w, r)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/login", 303)
		return
	}

	content := r.FormValue("content")

	err = posts.SubmitPost(user.ID, content)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	log.Println(r.RemoteAddr + " tweeted: " + content)

	http.Redirect(w, r, "/", 303)
}

func followHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Process follow " + r.Method + " request from " + r.RemoteAddr)
	err := r.ParseForm()

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user, err := users.ValidateTokenMiddleware(w, r)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/login", 303)
		return
	}

	content := r.FormValue("action")
	userID := r.FormValue("user")

	if content == "1" {
		err := user.Follow(userID)

		if err != nil {
			log.Println(err)
		}
		return
	}

	err = user.Unfollow(userID)

	if err != nil {
		log.Println(err)
	}
}

func profileHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Process profile " + r.Method + " request from " + r.RemoteAddr)
	var profileUser users.User
	vars := mux.Vars(r)
	user, err := users.ValidateTokenMiddleware(w, r)
	signin := err == nil

	profile, ok := vars["profile"]
	if !ok {
		if !signin {
			http.Redirect(w, r, "/login", 303)
		}

		profileUser = user
	} else {
		id, err := strconv.Atoi(profile)

		if err != nil {
			profileUser, err = users.UserByUsername(profile)

			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
		} else {
			profileUser, err = users.UserByID(id)

			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
		}
	}

	err = templates.ExecuteTemplate(w, "Profile", profilePage{Name: "Profile", ProfileUser: profileUser, User: user})

	if err != nil {
		log.Println(err)
	}
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Process logout" + r.Method + " request from " + r.RemoteAddr)
	expiration := time.Now().Add(1 * time.Hour)
	cookie := http.Cookie{Name: "token", Value: "", Expires: expiration}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/login", 303)
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Process login" + r.Method + " request from " + r.RemoteAddr)
	var errorMessage string

	_, err := users.ValidateTokenMiddleware(w, r)
	if err == nil {
		http.Redirect(w, r, "/", 303)
	}

	if r.Method == "POST" {
		err := r.ParseForm()

		if err != nil {
			log.Println("Error parsing login form")
			errorMessage = "Error parsing login form, please try again later"
		} else {
			username := r.FormValue("username")
			password := r.FormValue("password")

			token, err := users.GenerateToken(username, password)
			if err != nil {
				log.Println(err)
				errorMessage = fmt.Sprint(err)
			} else {
				expiration := time.Now().Add(365 * 24 * time.Hour)
				cookie := http.Cookie{Name: "token", Value: token, Expires: expiration}
				http.SetCookie(w, &cookie)
				http.Redirect(w, r, "/", 303)
			}
		}
	}

	err = templates.ExecuteTemplate(w, "Login", loginPage{Name: "Login", Error: errorMessage})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func initServer(port string) {
	r := mux.NewRouter()
	// mux := http.NewServeMux()

	r.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public/"))))

	r.Handle("/profile/{profile}", http.HandlerFunc(profileHandler))
	r.Handle("/profile", http.HandlerFunc(profileHandler))
	r.Handle("/follow", http.HandlerFunc(followHandler)).Methods("POST")
	r.Handle("/submit", http.HandlerFunc(tweetHandler)).Methods("POST")
	r.Handle("/login", http.HandlerFunc(loginHandler))
	r.Handle("/logout", http.HandlerFunc(logoutHandler)).Methods("GET")
	r.Handle("/", http.HandlerFunc(indexHandler)).Methods("GET")

	log.Println("Listening...")
	err := http.ListenAndServe(port, r)

	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	db.InitDB("./foo.db")
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	initServer(":3000")
}
