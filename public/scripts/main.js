$('#follow').on('click', function(e) {
    if(window.userID && typeof(window.follow) !== 'undefined' && window.username) {
        console.log('test');
        var request = new XMLHttpRequest();
        request.open('POST', 'http://localhost:3000/follow', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        if(window.follow) {
            request.send(`action=0&user=${window.userID}`);
            $('#follow').html(`Follow ${window.username}`);
        } else {
            request.send(`action=1&user=${window.userID}`);
            $('#follow').html(`Unfollow ${window.username}`);
            console.log($('#follow').html());
        }
        window.follow = !window.follow;
    }
});

function submitTweet() {
    $.ajax({
        method: 'POST', 
        url: 'http://' + window.location.host + '/submit',
        data: {
            content: $('#submit-post-content').val()
        },
        success: function() {
            window.location.reload()
        }
    });
}

$('#send-post').on('submit', e => {
    e.preventDefault();
    submitTweet();
});
