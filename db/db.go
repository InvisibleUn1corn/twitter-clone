package db

import (
	"database/sql"
	// We don't need access to sqlite3 driver's variables or functions
	_ "github.com/mattn/go-sqlite3"
	"log"
)

// Conn should be the only connection to the database
var Conn *sql.DB

// InitDB create a connection to the database
func InitDB(dataSourceName string) {
	database, err := sql.Open("sqlite3", dataSourceName)
	if err != nil {
		log.Panic(err)
	}

	if err = database.Ping(); err != nil {
		log.Panic(err)
	}

	Conn = database
}
