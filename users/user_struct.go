package users

import (
	"log"

	"gitlab.com/InvisibleUn1corn/image-board.git/db"
	"gitlab.com/InvisibleUn1corn/image-board.git/posts"
)

//User is a struct that contains all the data about the user
type User struct {
	ID       string
	Username string
	Email    string
}

//FollowedBy check if a userID is following current user
func (u User) FollowedBy(userID string) bool {
	var count int
	err := db.Conn.QueryRow("SELECT count(*) FROM followRelation WHERE followingID = ? AND followedID = ?", userID, u.ID).Scan(&count)

	if err != nil {
		log.Println(err)
		return false
	}

	return (count > 0)
}

//Following check if the current user is following userID
func (u User) Following(userID string) bool {
	var count int
	err := db.Conn.QueryRow("SELECT count(*) FROM followRelation WHERE followingID = ? AND followedID = ?", u.ID, userID).Scan(&count)

	if err != nil {
		log.Println(err)
		return false
	}

	return (count > 0)
}

//GetPostsCount count a user tweets and return an int represent the amount of tweets
func (u User) GetPostsCount() (int, error) {
	var count int
	err := db.Conn.QueryRow("SELECT count(*) FROM posts WHERE creatorID = ?", u.ID).Scan(&count)

	if err != nil {
		return 0, err
	}

	return count, err
}

//GetFollowersCount returns the nuber of followers of user u
func (u User) GetFollowersCount() int {
	var count int
	err := db.Conn.QueryRow("SELECT count(*) FROM followRelation WHERE followedID = ?", u.ID).Scan(&count)

	if err != nil {
		log.Println(err)
		return count
	}

	return count
}

//GetFollowingCount returns the number of users that user u is following
func (u User) GetFollowingCount() int {
	var count int
	err := db.Conn.QueryRow("SELECT count(*) FROM followRelation WHERE followingId= ?", u.ID).Scan(&count)

	if err != nil {
		log.Println(err)
		return count
	}

	return count
}

//GetPosts returns user posts
func (u User) GetPosts(count int) ([]posts.Post, error) {
	return posts.GetPostsByUser(u.ID, count)
}

//Follow a given userID
func (u User) Follow(userID string) error {
	stmt, err := db.Conn.Prepare("INSERT INTO followRelation (followingID, followedID) VALUES (?, ?)")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(u.ID, userID)
	if err != nil {
		return err
	}

	return nil
}

//Unfollow a given userID
func (u User) Unfollow(userID string) error {
	stmt, err := db.Conn.Prepare("DELETE FROM followRelation WHERE followingID = ? AND followedID = ?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(u.ID, userID)
	if err != nil {
		return err
	}

	return nil
}
