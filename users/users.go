package users

import (
	"errors"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/InvisibleUn1corn/image-board.git/db"
)

var key = []byte("MySecretKey")

// GenerateToken generate a token for a login user
func GenerateToken(first, password string) (string, error) {
	var id int
	var pass, username, secret string

	err := db.Conn.QueryRow("SELECT id, username, password, secret FROM users WHERE username = ? OR email = ?", first, first).Scan(&id, &username, &pass, &secret)

	if err != nil {
		return "", err
	}

	err = bcrypt.CompareHashAndPassword([]byte(pass), []byte(password))

	if err != nil {
		return "", errors.New("Wrong password")
	}

	token := jwt.New(jwt.SigningMethodHS256)

	t := token.Claims.(jwt.MapClaims)

	t["id"] = id
	t["username"] = username
	t["exp"] = time.Now().Add(time.Hour * 24 * 30).Unix()

	tokenString, err := token.SignedString(key)

	return tokenString, err
}

//UserByID returns a user with a given ID
func UserByID(id int) (User, error) {
	u := User{}

	err := db.Conn.QueryRow("SELECT id, username, email FROM users WHERE id = ?", id).Scan(&u.ID, &u.Username, &u.Email)

	if err != nil {
		return User{}, err
	}

	return u, nil
}

//UserByUsername returns a user with a given username
func UserByUsername(username string) (User, error) {
	u := User{}

	err := db.Conn.QueryRow("SELECT id, username, email FROM users WHERE username = ?", username).Scan(&u.ID, &u.Username, &u.Email)

	if err != nil {
		return User{}, err
	}

	return u, nil
}

func tokenToUser(t *jwt.Token) (User, error) {
	var m map[string]interface{}
	m = t.Claims.(jwt.MapClaims)

	if val, ok := m["id"]; ok {
		if id, ok := val.(float64); ok {
			return UserByID(int(id))
		}

		return User{}, errors.New("id is not a number")
	}

	return User{}, errors.New("id does not exist")
}

func validateTokenExp(*jwt.Token) error {
	return nil
}

//ValidateTokenMiddleware is a middleware that checks if the user is signed in
func ValidateTokenMiddleware(w http.ResponseWriter, r *http.Request) (User, error) {
	cookie, err := r.Cookie("token")

	if err != nil {
		return User{}, err
	}

	vals := strings.Split(cookie.String(), "=")

	if len(vals) < 2 {
		return User{}, errors.New("Cookie is broken")
	}

	token, err := jwt.Parse(vals[1], func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	if err == nil {
		if token.Valid {
			return tokenToUser(token)
		}

		return User{}, errors.New("Token is not valid")
	}

	log.Println(err)

	return User{}, errors.New("Unautorised access to this resource")
}
