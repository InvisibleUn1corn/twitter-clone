// Package posts provides an interface to interact with posts
package posts

import (
	"html"
	"time"

	"gitlab.com/InvisibleUn1corn/image-board.git/db"
)

//Post contains all the data about a post
type Post struct {
	ID          int
	CreatorID   int
	CreatorName string
	Time        int
	Parent      int
	Content     string
}

//GetPosts return the latest post for a user
func GetPosts(count int, userID string) ([]Post, error) {
	posts := []Post{}

	rows, err := db.Conn.Query("SELECT p.id, p.creatorID, p.time, p.parent, p.content, u.username FROM posts p INNER JOIN users u ON p.creatorID = u.id WHERE p.creatorID IN (SELECT followedID FROM followRelation WHERE followRelation.followingID = ?) OR p.creatorID == ? ORDER BY p.id DESC LIMIT ?", userID, userID, count)
	// rows, err := db.Conn.Query("SELECT p.id, p.creatorID, p.time, p.parent, p.content, u.username FROM posts p INNER JOIN users u ON p.creatorID = u.id ORDER BY p.id DESC LIMIT ?", count)
	if err != nil {
		return posts, err
	}
	defer rows.Close()

	for rows.Next() {
		p := Post{}
		err := rows.Scan(&p.ID, &p.CreatorID, &p.Time, &p.Parent, &p.Content, &p.CreatorName)
		if err != nil {
			return posts, err
		}

		posts = append(posts, p)
	}

	err = rows.Err()
	if err != nil {
		return posts, err
	}

	return posts, nil
}

//GetPostsByUser returns the latest posts of the user
func GetPostsByUser(userID string, count int) ([]Post, error) {
	posts := []Post{}

	rows, err := db.Conn.Query("SELECT p.id, p.creatorID, p.time, p.parent, p.content, u.username FROM posts p INNER JOIN users u ON p.creatorID = u.id WHERE creatorID = ? ORDER BY p.id DESC LIMIT ?", userID, count)
	if err != nil {
		return posts, err
	}
	defer rows.Close()

	for rows.Next() {
		p := Post{}
		err := rows.Scan(&p.ID, &p.CreatorID, &p.Time, &p.Parent, &p.Content, &p.CreatorName)
		if err != nil {
			return posts, err
		}

		posts = append(posts, p)
	}

	err = rows.Err()
	if err != nil {
		return posts, err
	}

	return posts, nil
}

//SubmitPost insert a post into the database and send notifications for mentioned users
func SubmitPost(userID string, content string) error {
	stmt, err := db.Conn.Prepare("INSERT INTO posts(creatorID, time, parent, content) VALUES(?, ?, 0, ?)")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(userID, int32(time.Now().Unix()), html.EscapeString(content))
	if err != nil {
		return err
	}

	return nil
}
